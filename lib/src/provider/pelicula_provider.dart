import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:peliculasapp/src/models/pelicula_model.dart';

class PeliculasProvider {
  String _apikey = 'b39da464a6b1510ce0b0cc170bffdd72';
  String _url = 'api.themoviedb.org';
  String _languaje = 'es-ES';

  Future<List<Pelicula>> getEnCines() async {
    final url = Uri.https(_url, '3/movie/now_playing',
        {'api_key': _apikey, 'languaje': _languaje});

    final resp = await http.get(url);
    final decodeData = json.decode(resp.body);
    print(decodeData);
  }
}
